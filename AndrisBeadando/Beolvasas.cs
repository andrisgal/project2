﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Beolvasas
    {
        static public void Beolvas(string fajl)
        {
            StreamReader reader = new StreamReader(fajl);
            int sorokSzama = 0;
            int uresSor = 0;
            while (!reader.EndOfStream) //Beolvassuk, hogy hány csúcsa lesz a gráfnak, azaz hány kompatibilitási lista van.
            {
                string sor = reader.ReadLine();
                if (sor.Length == 0) //Megvan a bemeneti fájlban az üres sor, innen kezdődnek a kompatibilitási listák.
                {
                    uresSor++;
                }
                else if (uresSor == 1)
                {
                    sorokSzama++; //Hány kompatibilitási lista van?
                }
            }
            Program.kompatibilisGraf.Setup(sorokSzama); //Beállítjuk a gráf csúcsait. Csúcsok száma = kompatibilitási listák száma.
            
            reader = new StreamReader(fajl); //Ismét betöltjük a fájlt.

            uresSor = 0;
            sorokSzama = 0;

            while (!reader.EndOfStream)
            {
                string sor = reader.ReadLine();
                string[] elemek = sor.Split(':'); //Tömbökbe rendettük a sorokat
                if (sor.Length == 0) //Ha elértünk az üres sorhoz, akkor váltunk a beolvasási módon, és elkezdjük a kompatibilitási listákat tárolni.
                {
                    uresSor++;
                }
                else if (uresSor == 0) //Ha még nem értük el az üres sort, akkor előbb a megadott hardvereket olvassuk be.
                {
                    if (int.Parse(elemek[3]) == 0)
                    {
                        if (elemek[4] == "Intel")
                        {
                            Program.hardverElemek.AddLast(new IntelProcesszor(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                        }
                        else if (elemek[4] == "AMD")
                        {
                            Program.hardverElemek.AddLast(new AMDProcesszor(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                        }
                    }
                    else if (int.Parse(elemek[3]) == 1)
                    {
                        Program.hardverElemek.AddLast(new Alaplap(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                    }
                    else if (int.Parse(elemek[3]) == 2)
                    {
                        Program.hardverElemek.AddLast(new Huto(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                    }
                    else if (int.Parse(elemek[3]) == 3)
                    {
                        Program.hardverElemek.AddLast(new Ram(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                    }
                    else if (int.Parse(elemek[3]) == 4)
                    {
                        Program.hardverElemek.AddLast(new Tapegyseg(elemek[0], int.Parse(elemek[1]), int.Parse(elemek[2]), (Tipus)int.Parse(elemek[3])));
                    }
                }
                else if (uresSor == 1) //Ha elértük az üres sort, akkor a kompatibilitási listákat olvassuk be.
                {
                    for (int i = 0; i < elemek.Length; i++)
                    {Program.kompatibilisGraf.Beszuras(elemek[i], sorokSzama); //A sorszám a gráf csúcsa, ahova beszúrjuk sorba a ':'-tal elválasztott elemeket egyesével.
                    }
                    sorokSzama++;
                }
            }
            reader.Close();
        }
    }
}
