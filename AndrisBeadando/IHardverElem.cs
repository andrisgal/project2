﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    interface IHardverElem
    {
        string Nev { get; set; }
        int Minoseg { get; set; }
        int Ar { get; set; }
        Tipus Tipus { get; set; }

        void Beepit(Szamitogep szgep);
        void Elromlik();
    }
}
