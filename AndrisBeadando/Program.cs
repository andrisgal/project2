﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Program
    {
        public static LinkedList<IHardverElem> hardverElemek;
        public static Graf<string> kompatibilisGraf;
        static void Main(string[] args)
        {
            hardverElemek = new LinkedList<IHardverElem>();
            kompatibilisGraf = new Graf<string>();
            Beolvasas.Beolvas("bemenet.txt"); //Beolvassuk a hardver elemeket és a kompatibilitási listákat gráfokba.

            Console.WriteLine("Az elérhető eszközök: ");
            LinkedListNode<IHardverElem> fej = hardverElemek.First;
            while (fej != null)
            {
                Console.WriteLine("   " + fej.Value.Nev + " - Minőség: " + fej.Value.Minoseg + ", típus: " + fej.Value.Tipus + ", Ár: " + fej.Value.Ar + " Ft");
                fej = fej.Next;
            }
            Console.WriteLine();
            Console.WriteLine("Kompatibilis eszközpárok:");
            kompatibilisGraf.Kiiras();

            try
            {
                string[] parameterek = new string[5];
                Console.WriteLine("A paraméterek megadásánál írj '-' jelet, ha nem akarod az adott paramétert kiválasztani.");
                for (int i = 0; i < parameterek.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            Console.Write("Add meg a kívánt processzor nevét, vagy hagyd üresen: ");
                            break;
                        case 1:
                            Console.Write("Add meg a kívánt alaplap nevét, vagy hagyd üresen: ");
                            break;
                        case 2:
                            Console.Write("Add meg a kívánt hűtő nevét, vagy hagyd üresen: ");
                            break;
                        case 3:
                            Console.Write("Add meg a kívánt ram nevét, vagy hagyd üresen: ");
                            break;
                        case 4:
                            Console.Write("Add meg a kívánt tápegység nevét, vagy hagyd üresen: ");
                            break;
                        default:
                            break;
                    }
                    parameterek[i] = Console.ReadLine();
                }

                Console.Write("Ár szerint (0) vagy minőség szerint (1) szeretnéd megkapni a legjobb találatot? ");
                int szempont = int.Parse(Console.ReadLine());
                if (szempont > 1 || szempont < 0)
                {
                    szempont = 0;
                }

                Szamitogep[] keresettGepek = OptimalisKonfiguracio(parameterek, szempont); //A tömb 0. eleme az optimális, a többi eleme (ha van) akkor a nem optimális, csak a lehetséges megoldás.
                if (keresettGepek[0] != null)
                {
                    Console.WriteLine();
                    Console.WriteLine("A(z) " + (szempont == 0 ? "ár" : "minőség") + " szerinti elvárásoknak megfelelő konfiguráció:");
                    for (int i = 0; i < keresettGepek.Length; i++)
                    {
                        Console.WriteLine();
                        if (i == 0)
                        {
                            Console.WriteLine("  -=[ A LEGOPTIMÁLISABB MEGOLDÁS ]=-");
                        }
                        else
                        {
                            Console.WriteLine("  -=[ Egyéb lehetséges megoldás ]=-");
                        }
                        Console.WriteLine();

                        if (keresettGepek[i].Processzor != null)
                        {
                            Console.WriteLine("  Processzor: " + keresettGepek[i].Processzor.Nev + " - " + keresettGepek[i].Processzor.Ar + " Ft, Minőség: " + keresettGepek[i].Processzor.Minoseg);
                        }
                        if (keresettGepek[i].Alaplap != null)
                        {
                            Console.WriteLine("  Alaplap: " + keresettGepek[i].Alaplap.Nev + " - " + keresettGepek[i].Alaplap.Ar + " Ft, Minőség: " + keresettGepek[i].Alaplap.Minoseg);
                        }
                        if (keresettGepek[i].Huto != null)
                        {
                            Console.WriteLine("  Hűtő: " + keresettGepek[i].Huto.Nev + " - " + keresettGepek[i].Huto.Ar + " Ft, Minőség: " + keresettGepek[i].Huto.Minoseg);
                        }
                        if (keresettGepek[i].Ram != null)
                        {
                            Console.WriteLine("  Ram: " + keresettGepek[i].Ram.Nev + " - " + keresettGepek[i].Ram.Ar + " Ft, Minőség: " + keresettGepek[i].Ram.Minoseg);
                        }
                        if (keresettGepek[i].Tapegyseg != null)
                        {
                            Console.WriteLine("  Tápegység: " + keresettGepek[i].Tapegyseg.Nev + " - " + keresettGepek[i].Tapegyseg.Ar + " Ft, Minőség: " + keresettGepek[i].Tapegyseg.Minoseg);
                        }
                        Console.WriteLine("  --------------------");
                        Console.WriteLine("  Minőség: " + keresettGepek[i].Minoseg);
                        Console.WriteLine("  Ár: " + keresettGepek[i].Ar + " Ft");
                    }
                }
                else
                {
                    Console.WriteLine("Nem található az elvárásoknak megfelelő konfiguráció.");
                }
            }
            catch (NincsTalalatKivetel kivetel)
            {
                Console.WriteLine(kivetel.Message);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        private static Szamitogep[] OptimalisKonfiguracio(string[] parameterek, int szempont)
        {
            Szamitogep[] keresettGepek = null;
            if (KompatibilisParameterek(parameterek).First != null) //Ha vannak kompatibilis paraméterek a megadottak mellett.
            {
                LinkedList<LinkedList<IHardverElem>> potencialisHardverek = KeresesErtekAlapjan(parameterek); //Listába szedjük azokat a hardver listákat, amik megfelelnek számunkra.
                if (potencialisHardverek != null)
                {
                    int kompListak = KompatibilisListakSzama(potencialisHardverek.First); //Megnézzük, hogy hány kompatibilis hardverekből álló listánk van.
                    int i = 1; //Alapból az 1. kompatibilis elemekből álló listát elemezzük.
                    Szamitogep[] szgepek = new Szamitogep[kompListak]; //Számítógép tömb, legfeljebb akkora méretben, ahogy kompatibilitási listánk van.
                    while (i <= kompListak) //Ahány kompatibilis listánk van, addig mindegyik listát egyesével kielemezzük.
                    {
                        int[] M = new int[5]; //Lehetséges opciók hardvertípusonként
                        IHardverElem[] E = new IHardverElem[5]; //A végső megoldás, ez tartalmazza majd a visszalépéses keresés után, hogy a jelenleg elemezendő hardverlistába mely hadrverek kerülhetnek bele ár/minőség alapján.
                        IHardverElem[,] R = new IHardverElem[5, 100]; //Sorai: hardvertípusok; Oszlopai (ezt a számot random választottam): egy adott hardvertípushoz tartozó hardverek. Pl ha több processzor van, akkor ezeket az oszlopokban sorakoztatjuk fel.

                        LinkedListNode<IHardverElem> fej = hardverElemek.First; //A beolvasott hardverek listája.
                        while (fej != null)
                        {
                            if (ListabanKereses(potencialisHardverek.First, fej.Value.Nev, i)) //Ha megtallálunk egy elemet a jelenlegi kompatibilitási listában
                            {
                                R[(int)fej.Value.Tipus, M[(int)fej.Value.Tipus]++] = fej.Value; //Akkor belerakjuk
                            }
                            fej = fej.Next;
                        }

                        Backtracking<IHardverElem> btr = new Backtracking<IHardverElem>(M, E, R); //Létrehozunk egy backtrackinget
                        bool van = false; //Van-e találat
                        if (szempont == 0) //Ár szerinti keresés
                        {
                            btr.VisszalepesesKeresesOptAr(0, ref van);
                        }
                        else if (szempont == 1) //Minőség szerinti keresés
                        {
                            btr.VisszalepesesKeresesOptMinoseg(0, ref van);
                        }
                        
                        if (van) //Ha van találat
                        {
                            szgepek[i - 1] = new Szamitogep(); //Létrehozunk a kiszűrt elemekből egy számítógépet.
                            for (int k = 0; k < btr.E.Length; k++)
                            {
                                if (parameterek[k] != "-") //Ha az adott paraméterre nem mondtuk, hogy nem szeretnénk venni
                                {
                                    E[k].Beepit(szgepek[i - 1]);
                                }
                            }
                        }
                        i++;
                    }
                    keresettGepek = new Szamitogep[szgepek.Length];
                    Szamitogep keresettGep = null;
                    if (szempont == 0) //Ár szerinti összehasonlítása a lehetséges számítógépeknek.
                    {
                        keresettGep = LegolcsobbKonfiguracio(szgepek);
                    }
                    else if (szempont == 1) //Minőség szerinti összehasonlítása a lehetséges számítógépeknek.
                    {
                        keresettGep = LegminosegibbKonfiguracio(szgepek);
                    }
                    keresettGepek[0] = keresettGep; //A tömb első eleme a legjobb megoldás lesz
                    int idx = 1;
                    for (int m = 0; m < szgepek.Length; m++)//A többi eleme pedig az amúgy nem legoptimálisabb, ám lehetséges megoldások.
                    {
                        if (szgepek[m] != null && szgepek[m] != keresettGep) //Ha nem null és nem a keresett gép, amit már beraktunk, akkor berakja.
                        {
                            keresettGepek[idx] = szgepek[m];
                            idx++;
                        }
                    }
                }
            }
            return keresettGepek;
        }

        public static LinkedList<LinkedList<IHardverElem>> KeresesErtekAlapjan(string[] parameterek)
        {
            LinkedList<int> grafCsucsok = KompatibilisParameterek(parameterek); //A kompatibilitási gráf azon csúcsai, amik tartalmazzák a kért paramétereket.

            LinkedList<LinkedList<IHardverElem>> potencialisHardverek = new LinkedList<LinkedList<IHardverElem>>(); //Olyan láncolt lista, melynek minden eleme egy olyan láncolt lista, amely megfelelő hardver elemeket tartalmaz.
            LinkedListNode<IHardverElem> hardverFej = null;

            LinkedListNode<int> grafCsucsFej = grafCsucsok.First;
            while (grafCsucsFej != null) //Végig megyünk a kompatibilitási gráf azon csúcsain, amelyek tartalmazhatnak megfelelő hardvereket.
            {
                LinkedList<IHardverElem> hardverLista = new LinkedList<IHardverElem>(); //Hardver elemekből álló lista.
                bool[] mindenHardverVanE = new bool[parameterek.Length]; //Tárolja, hogy minden kért hardver elem megvan-e.

                hardverFej = hardverElemek.First; //Az eredeti hardver lista feje.
                while (hardverFej != null) //Végiglépegetünk az egészen.
                {
                    if (kompatibilisGraf.BenneVan(grafCsucsFej.Value, hardverFej.Value.Nev) &&
                        (parameterek[(int)hardverFej.Value.Tipus] == "" || parameterek[(int)hardverFej.Value.Tipus] == "-" || parameterek[(int)hardverFej.Value.Tipus] == hardverFej.Value.Nev))
                    //Ellenőrizzük, hogy a kompatibilitást tároló gráfban az adott csúcson (grafCsucsFej.Value) megtalálható-e az adott paraméter (ha megvan adva paraméter, tehát nem üres)
                    {
                        if (!mindenHardverVanE[(int)hardverFej.Value.Tipus]) //Ha még nincs ilyen hardverünk
                        {
                            mindenHardverVanE[(int)hardverFej.Value.Tipus] = true; //Akkor mostmár van
                        }
                        hardverLista.AddLast(hardverFej.Value); //Hozzáadjuk a hardverlistánkhoz opcióként.
                    }
                    hardverFej = hardverFej.Next;
                }
                if (MindenHardverSzerepel(mindenHardverVanE)) //Ha minden kért paramétert teljesítettünk, és minden hardvertípusból találtunk kompatibiliset.
                {
                    if (hardverLista.Count > 0) //Ha a hardverlistánk nem üres
                    {
                        potencialisHardverek.AddLast(hardverLista); //Akkor hozzáadjuk ezt a listát a potenciális listánkhoz.
                    }
                }

                grafCsucsFej = grafCsucsFej.Next;
            }
            return potencialisHardverek;
        }

        static Szamitogep LegolcsobbKonfiguracio(Szamitogep[] szgepek)
        {
            int legolcsobbIdx = 0;
            for (int i = 1; i < szgepek.Length; i++)
            {
                if (szgepek[i] != null && szgepek[i].Ar < szgepek[legolcsobbIdx].Ar)
                {
                    legolcsobbIdx = i;
                }
            }
            return szgepek[legolcsobbIdx];
        }

        static Szamitogep LegminosegibbKonfiguracio(Szamitogep[] szgepek)
        {
            int legjobbIdx = 0;
            for (int i = 1; i < szgepek.Length; i++)
            {
                if (szgepek[i] != null && szgepek[i].Minoseg > szgepek[legjobbIdx].Minoseg)
                {
                    legjobbIdx = i;
                }
            }
            return szgepek[legjobbIdx];
        }

        static bool ListabanKereses(LinkedListNode<LinkedList<IHardverElem>> fej, string mit, int hanyadikKompatibilitasiListaban)
        {
            int szam = 1;

            while (fej != null && szam < hanyadikKompatibilitasiListaban)
            {
                szam++;
                fej = fej.Next;
            }

            LinkedListNode<IHardverElem> fej2 = fej.Value.First;
            bool talalat = false;
            while (fej2 != null && !talalat)
            {
                if (fej2.Value.Nev == mit)
                {
                    talalat = true;
                }
                fej2 = fej2.Next;
            }
            return talalat;
        }

        static int KompatibilisListakSzama(LinkedListNode<LinkedList<IHardverElem>> fej)
        {
            int szam = 0;
            while (fej != null)
            {
                szam++;
                fej = fej.Next;
            }
            return szam;
        }

        static bool MindenHardverSzerepel(bool[] mindenHardverVanE)
        {
            bool igen = true;
            int i = 0;
            while (i < mindenHardverVanE.Length && !igen)
            {
                if (mindenHardverVanE[i] == false)
                {
                    igen = false;
                }
                i++;
            }

            return igen;
        }

        static LinkedList<int> KompatibilisParameterek(string[] parameterek)
        {
            LinkedList<int> kompGrafCsucsok = new LinkedList<int>();
            int megadottParameterek = MegadottParameterek(parameterek); //Konkrétan megadott paraméterek száma.
            int talalat = 0;

            int i = 0;
            while (i < kompatibilisGraf.csucsok.Length) //Végig meg a kompatibilitási lista csúcsain (minden csúcs egy láncolt lista a kompatibilis elemekkel)
            {
                int j = 0;
                while (j < parameterek.Length && talalat != megadottParameterek) //Végig megy a paramétereken, amíg meg nem találja a megfelelő számú paramétereket.
                {
                    if (kompatibilisGraf.BenneVan(i, parameterek[j]))
                    {
                        talalat++;
                    }
                    j++;
                }
                if (talalat == megadottParameterek) //Ha minden paramétert megtalált a kompatibilitási listában.
                {
                    kompGrafCsucsok.AddLast(i); //Akkor hozzáadja ezt a csúcs számot. Ezzel a csúccsal rendelkező gráf tartalmaz lehetséges opciókat.
                }
                talalat = 0;
                i++;
            }
            
            return kompGrafCsucsok;
        }

        public static int MegadottParameterek(string[] parameterek) //Visszaadja, hogy hány paramétert adutnk meg fixen a kereséshez.
        {
            int kitetelek = 0;
            for (int i = 0; i < parameterek.Length; i++)
            {
                if (parameterek[i] != "" && parameterek[i] != "-")
                {
                    kitetelek++;
                }
            }
            return kitetelek;
        }
    }
}
