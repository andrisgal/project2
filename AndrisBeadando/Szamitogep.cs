﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Szamitogep
    {
        public Alaplap Alaplap { get; set; }
        public Processzor Processzor { get; set; }
        public Ram Ram { get; set; }
        public Huto Huto { get; set; }
        public Tapegyseg Tapegyseg { get; set; }

        public int Ar { get { return (Processzor == null ? 0 : Processzor.Ar) + (Alaplap == null ? 0 : Alaplap.Ar) + (Huto == null ? 0 : Huto.Ar) + (Ram == null ? 0 : Ram.Ar) + (Tapegyseg == null ? 0 : Tapegyseg.Ar); } }
        public int Minoseg { get { return (Processzor == null ? 0 : Processzor.Minoseg) + (Alaplap == null ? 0 : Alaplap.Minoseg) + (Huto == null ? 0 : Huto.Minoseg) + (Ram == null ? 0 : Ram.Minoseg) + (Tapegyseg == null ? 0 : Tapegyseg.Minoseg); } }

        public delegate void Elromlik(IHardverElem hardver);
        public Elromlik elromlik;

        public Szamitogep()
        {
            elromlik += Elromlott;
        }

        void Elromlott(IHardverElem hardver)
        {
            Console.WriteLine("A számítógép '" + hardver.Tipus + "' típusú alkatrésze elromlott!");
        }
    }
}
