﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Graf<T>
    {
        public LinkedList<T>[] csucsok { get; private set; }

        public void Setup(int elemszam)
        {
            csucsok = new LinkedList<T>[elemszam];
            for (int i = 0; i < elemszam; i++)
            {
                csucsok[i] = new LinkedList<T>();
            }
        }

        public void Beszuras(T hardverNeve, int csucs)
        {
            csucsok[csucs].AddLast(hardverNeve);
        }

        public void Torles(T hardverNeve, int csucs)
        {
            csucsok[csucs].Remove(hardverNeve);
        }

        public void Kiiras()
        {
            for (int csucs = 0; csucs < Meret(); csucs++)
            {
                LinkedListNode<T> fej = csucsok[csucs].First;
                while (fej != null)
                {
                    string elvalaszto = " - ";
                    if (fej.Next == null)
                    {
                        elvalaszto = "";
                    }
                    Console.Write(fej.Value.ToString() + elvalaszto);
                    fej = fej.Next;
                }
                Console.WriteLine();
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void Kiiras(int csucs)
        {
            LinkedListNode<T> fej = csucsok[csucs].First;
            while (fej != null)
            {
                Console.Write(fej.Value.ToString() + " - ");
                fej = fej.Next;
            }
            Console.WriteLine();
        }

        public bool BenneVan(int csucs, string mi)
        {
            bool benne = false;
            LinkedListNode<T> fej = csucsok[csucs].First;
            while (fej != null && !benne)
            {
                if (fej.Value.ToString() == mi)
                {
                    benne = true;
                }
                fej = fej.Next;
            }
            return benne;
        }

        public int Meret()
        {
            return csucsok.Length;
        }
    }
}
