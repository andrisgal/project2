﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Ram : IHardverElem
    {
        public string Nev { get; set; }
        public int Minoseg { get; set; }
        public int Ar { get; set; }
        public Tipus Tipus { get; set; }
        LinkedList<Szamitogep> Szamitogepek = new LinkedList<Szamitogep>(); //Gépek, ahová be van építve ez a hardver.

        public Ram(string nev, int minoseg, int ar, Tipus tipus)
        {
            Nev = nev;
            Minoseg = minoseg;
            Ar = ar;
            Tipus = tipus;
        }

        public void Beepit(Szamitogep szgep)
        {
            szgep.Ram = this;
            Szamitogepek.AddLast(szgep);
        }

        public void Elromlik()
        {
            LinkedListNode<Szamitogep> szamitogepFej = Szamitogepek.First;
            while (szamitogepFej != null)
            {
                szamitogepFej.Value.elromlik(this);
                szamitogepFej = szamitogepFej.Next;
            }
        }
    }
}
