﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndrisBeadando
{
    class Backtracking<T> where T : IHardverElem
    {
        public int[] M { get; private set; }
        public T[] E { get; private set; }
        public T[,] R { get; private set; }

        public Backtracking(int[] M, T[] E, T[,] R)
        {
            this.M = M;
            this.E = E;
            this.R = R;
        }

        public void VisszalepesesKeresesOptAr(int szint, ref bool van)
        {
            int i = 0;
            while (i < M[szint])
            {
                if (Ft(szint, R[szint, i]))
                {
                    if (FkOptAr(szint, R[szint, i]))
                    {
                        E[szint] = R[szint, i];
                        if (szint == M.Length - 1)
                        {
                            van = true;
                        }
                        else
                        {
                            VisszalepesesKeresesOptAr(szint + 1, ref van);
                        }
                    }
                }
                i++;
            }
        }

        public void VisszalepesesKeresesOptMinoseg(int szint, ref bool van)
        {
            int i = 0;
            while (i < M[szint])
            {
                if (Ft(szint, R[szint, i]))
                {
                    if (FkOptMinoseg(szint, R[szint, i]))
                    {
                        E[szint] = R[szint, i];
                        if (szint == M.Length - 1)
                        {
                            van = true;
                        }
                        else
                        {
                            VisszalepesesKeresesOptAr(szint + 1, ref van);
                        }
                    }
                }
                i++;
            }
        }

        bool FkOptAr(int szint, T v)
        {
            return E[szint] == null || E[szint].Ar > v.Ar;
        }

        bool FkOptMinoseg(int szint, T v)
        {
            return E[szint] == null || E[szint].Minoseg < v.Minoseg;
        }

        bool Ft(int szint, T v)
        {
            return !v.Equals(default(T));
        }
    }
}
